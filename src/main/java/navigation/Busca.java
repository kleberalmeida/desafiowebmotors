package navigation;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;

public class Busca {

    public static void filtroDeBusca(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        SetValues.setValores(sttp,lv,el,"selecionar todas versões","xpath",el.getSelecionartodasversoes(),"","click","Falha no clique do campo");
        SetValues.setValores(sttp,lv,el,"selecionar versão do veículo","xpath",el.getSelecionarversao(),"","click","Falha no clique do campo");
        SetValues.setValores(sttp,lv,el,"selecionar veículos novos","xpath",el.getSelecionarcarrosnovos(),"","click","Falha no clique do campo");
        SetValues.setValores(sttp,lv,el,"selecionar veículos usados","xpath",el.getSelecionarcarrosusados(),"","click","Falha no clique do campo");
        SetValues.setValores(sttp,lv,el,"selecionar concessionaria","xpath",el.getSelecionarconcessionaria(),"","click","Falha no clique do campo");
        SetValues.setValores(sttp,lv,el,"selecionar loja","xpath",el.getSelecionarloja(),"","click","Falha no clique do campo");
   }
}
