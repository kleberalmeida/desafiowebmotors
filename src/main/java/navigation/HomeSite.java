package navigation;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;

public class HomeSite {


    public static void pesquisarVeiculo(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        SetValues.setValores(sttp, lv, el, "campo pesquisa", "id", el.getCampopesquisa(), sttp.getTableLine()[0], "sendKeys", "Falha no preenchimento do campo");
        SetValues.setValores(sttp, lv, el, "selecionar veiculo", "xpath", el.getSelecionarveiculo(), "", "click", "Falha no clique do campo");
    }
}
