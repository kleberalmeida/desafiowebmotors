package common.start;

public class Element {

    private String campopesquisa;
    private String selecionarveiculo;
    private String selecionartodasversoes;
    private String selecionarversao;
    private String selecionarcarrosnovos;
    private String selecionarcarrosusados;
    private String selecionarconcessionaria;
    private String selecionarloja;

    public String getCampopesquisa() {
        return campopesquisa;
    }

    public void setCampopesquisa(String campopesquisa) {
        this.campopesquisa = campopesquisa;
    }

    public String getSelecionarveiculo() {
        return selecionarveiculo;
    }

    public void setSelecionarveiculo(String selecionarveiculo) {
        this.selecionarveiculo = selecionarveiculo;
    }

    public String getSelecionartodasversoes() {
        return selecionartodasversoes;
    }

    public void setSelecionartodasversoes(String selecionartodasversoes) {
        this.selecionartodasversoes = selecionartodasversoes;
    }

    public String getSelecionarversao() {
        return selecionarversao;
    }

    public void setSelecionarversao(String selecionarversao) {
        this.selecionarversao = selecionarversao;
    }

    public String getSelecionarcarrosnovos() {
        return selecionarcarrosnovos;
    }

    public void setSelecionarcarrosnovos(String selecionarcarrosnovos) {
        this.selecionarcarrosnovos = selecionarcarrosnovos;
    }

    public String getSelecionarcarrosusados() {
        return selecionarcarrosusados;
    }

    public void setSelecionarcarrosusados(String selecionarcarrosusados) {
        this.selecionarcarrosusados = selecionarcarrosusados;
    }

    public String getSelecionarconcessionaria() {
        return selecionarconcessionaria;
    }

    public void setSelecionarconcessionaria(String selecionarconcessionaria) {
        this.selecionarconcessionaria = selecionarconcessionaria;
    }

    public String getSelecionarloja() {
        return selecionarloja;
    }

    public void setSelecionarloja(String selecionarloja) {
        this.selecionarloja = selecionarloja;
    }
}
