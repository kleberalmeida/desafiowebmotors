package common.start;

public class LocalVariables {

    private String robotActions;
    private String robotAttribute;
    private String robotMessage;
    private String robotNameElement;
    private String robotValueField;
    private String robotValueAttribute;


    private String pathEvidence;
    private String elementType;
    private String result;
    private String captureText;
    private String status;
    private String identifileType;
    private String actionType;
    boolean returnExecute;

    public String getRobotActions() {
        return robotActions;
    }

    public void setRobotActions(String robotActions) {
        this.robotActions = robotActions;
    }

    public String getRobotAttribute() {
        return robotAttribute;
    }

    public void setRobotAttribute(String robotAttribute) {
        this.robotAttribute = robotAttribute;
    }

    public String getRobotMessage() {
        return robotMessage;
    }

    public void setRobotMessage(String robotMessage) {
        this.robotMessage = robotMessage;
    }

    public String getRobotNameElement() {
        return robotNameElement;
    }

    public void setRobotNameElement(String robotNameElement) {
        this.robotNameElement = robotNameElement;
    }

    public String getRobotValueField() {
        return robotValueField;
    }

    public void setRobotValueField(String robotValueField) {
        this.robotValueField = robotValueField;
    }

    public String getPathEvidence() {
        return pathEvidence;
    }

    public void setPathEvidence(String pathEvidence) {
        this.pathEvidence = pathEvidence;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCaptureText() {
        return captureText;
    }

    public void setCaptureText(String captureText) {
        this.captureText = captureText;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdentifileType() {
        return identifileType;
    }

    public void setIdentifileType(String identifileType) {
        this.identifileType = identifileType;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public boolean isReturnExecute() {
        return returnExecute;
    }

    public void setReturnExecute(boolean returnExecute) {
        this.returnExecute = returnExecute;
    }

    public String getRobotValueAttribute() {
        return robotValueAttribute;
    }

    public void setRobotValueAttribute(String robotValueAttribute) {
        this.robotValueAttribute = robotValueAttribute;
    }
}
