package common.utils;

import common.start.StartParameters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureDate {

    public static void captureNowDate(StartParameters sttp) {

        DateFormat dateformat = new SimpleDateFormat("yyyy MM dd - hh - mm");
        Date date = new Date();
        sttp.setDateNow(dateformat.format(date));
    }
}
