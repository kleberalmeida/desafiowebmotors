package common.utils;

import common.start.Element;

import java.io.IOException;

public class MapearElementos {

    public static void elementos (Element el) throws IOException, InterruptedException {

        el.setCampopesquisa("searchBar");
        el.setSelecionarveiculo("//div[@class='SearchBar__results__result__name']");
        el.setSelecionartodasversoes("//div[2]/div[3]");
        el.setSelecionarversao("//a[contains(.,'1.5 16V TURBO GASOLINA TOURING 4P CVT')]");
        el.setSelecionarcarrosnovos("//label[contains(.,'Novos')]");
        el.setSelecionarcarrosusados("//label[contains(.,'Usados')]");
        el.setSelecionarconcessionaria("//label[contains(.,'Concessionária')]");
        el.setSelecionarloja("//label[contains(.,'Loja')]");
    }
}