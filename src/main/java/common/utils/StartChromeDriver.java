package common.utils;

import common.start.StartParameters;
import org.openqa.selenium.chrome.ChromeDriver;

public class StartChromeDriver {

    public static void openChromeDriver(StartParameters sttp) {

        System.setProperty("webdriver.chrome.driver", sttp.getProp().getProperty("pathchromedriver"));

        sttp.setDriver(new ChromeDriver());
        sttp.getDriver().manage().window().maximize();
        sttp.getDriver().get(sttp.getProp().getProperty("url"));
    }
}
