package runner;

import common.properties.SetupProperties;
import common.start.StartParameters;
import common.utils.CaptureDate;
import common.utils.Folders;

import java.io.IOException;

public class SetupAutomation {

    public static void initAutomation(StartParameters sttp) throws IOException {

        SetupProperties.setConfig(sttp);
        CaptureDate.captureNowDate(sttp);
        Folders.generateRootFolder(sttp);
        Folders.generateSubFolder(sttp);

    }
}
