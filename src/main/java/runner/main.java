package runner;

import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;
import common.utils.MapearElementos;

public class main {

    public static void main(String[] args) throws Exception {

        StartParameters sttp = new StartParameters();
        LocalVariables lv = new LocalVariables();
        Element el = new Element();

        SetupAutomation.initAutomation(sttp);
        MapearElementos.elementos(el);
        Manager.managerExecution(sttp, lv, el);


    }
}
