package actions;

import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;

import java.io.IOException;

public class SelectActions {

    public static boolean efetiveActions(StartParameters sttp, LocalVariables lv, Element el) throws IOException, InterruptedException {

        DefiniActions.definiActions(lv);
        if(ExecuteActions.executeActions(sttp, lv, el) == false) return false;

        return true;
    }

}
