package actions;

import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

public class ExecuteActions {

    public static boolean executeActions(StartParameters sttp, LocalVariables lv, Element el) throws InterruptedException, IOException {

        // Name

        if (lv.getIdentifileType().equals("name")) {
            if (ValidateElementExistence.verifyByName(sttp, lv)) {
                if (lv.getActionType().equals("sendKeys")) {
                    sttp.getDriver().findElement(By.name(lv.getRobotValueAttribute())).sendKeys(lv.getRobotValueField());



                } else if (lv.getActionType().equals("click")) {
                    sttp.getDriver().findElement(By.name(lv.getRobotValueAttribute())).click();

                } else if (lv.getActionType().equals("select")) {
                    WebElement element = sttp.getDriver().findElement(By.name(lv.getRobotValueAttribute()));
                    Select select = new Select(element);
                    select.selectByValue(lv.getRobotValueField());
                } else if (lv.getActionType().equals(("getText"))) {
                    sttp.getDriver().findElement(By.name(lv.getRobotValueAttribute())).getText();
                }
            }else{
                    lv.setStatus("erro");
                    Evidence.evidenceResult(sttp, lv);
                    return false;
                }

        }


        //xpath

        if (lv.getIdentifileType().equals("xpath")) {
            if (ValidateElementExistence.verifyByXpath(sttp, lv)) {
                if (lv.getActionType().equals("sendKeys")) {
                    sttp.getDriver().findElement(By.xpath(lv.getRobotValueAttribute())).sendKeys(lv.getRobotValueField());

                } else if (lv.getActionType().equals("click")) {
                    sttp.getDriver().findElement(By.xpath(lv.getRobotValueAttribute())).click();
                } else if (lv.getActionType().equals("select")) {
                    WebElement element = sttp.getDriver().findElement(By.xpath(lv.getRobotValueAttribute()));
                    Select select = new Select(element);
                    select.selectByValue(lv.getRobotValueField());
                } else if (lv.getActionType().equals(("getText"))) {
                    sttp.getDriver().findElement(By.xpath(lv.getRobotValueAttribute())).getText();

                }
            }else {
                lv.setStatus("erro");
                Evidence.evidenceResult(sttp, lv);
                return false;
            }
        }

        //id

        if (lv.getIdentifileType().equals("id")) {
            if (ValidateElementExistence.verifyById(sttp, lv)) {
                if (lv.getActionType().equals("sendKeys")) {
                    sttp.getDriver().findElement(By.id(lv.getRobotValueAttribute())).sendKeys(lv.getRobotValueField());

                } else if (lv.getActionType().equals("click")) {
                    sttp.getDriver().findElement(By.id(lv.getRobotValueAttribute())).click();
                } else if (lv.getActionType().equals("select")) {
                    WebElement element = sttp.getDriver().findElement(By.id(lv.getRobotValueAttribute()));
                    Select select = new Select(element);
                    select.selectByValue(lv.getRobotValueField());
                } else if (lv.getActionType().equals(("getText"))) {
                    sttp.getDriver().findElement(By.id(lv.getRobotValueAttribute())).getText();

                }
            } else {
                lv.setStatus("erro");
                Evidence.evidenceResult(sttp, lv);
                return false;
            }
        }

        //linktext

        if (lv.getIdentifileType().equals("linktext")) {
            if (ValidateElementExistence.verifyByLinkText(sttp, lv)) {
                if (lv.getActionType().equals("sendKeys")) {
                    sttp.getDriver().findElement(By.linkText(lv.getRobotValueAttribute())).sendKeys(lv.getRobotValueField());

                } else if (lv.getActionType().equals("click")) {
                    sttp.getDriver().findElement(By.linkText(lv.getRobotValueAttribute())).click();
                } else if (lv.getActionType().equals("select")) {
                    WebElement element = sttp.getDriver().findElement(By.linkText(lv.getRobotValueAttribute()));
                    Select select = new Select(element);
                    select.selectByValue(lv.getRobotValueField());
                } else if (lv.getActionType().equals(("getText"))) {
                    sttp.getDriver().findElement(By.linkText(lv.getRobotValueAttribute())).getText();
                }

            } else {
                lv.setStatus("erro");
                Evidence.evidenceResult(sttp, lv);
                return false;
            }
        }

        //css

        if (lv.getIdentifileType().equals("css")) {
            if (ValidateElementExistence.verifyByCss(sttp, lv)) {
                if (lv.getActionType().equals("sendKeys")) {
                    sttp.getDriver().findElement(By.cssSelector(lv.getRobotValueAttribute())).sendKeys(lv.getRobotValueField());

                } else if (lv.getActionType().equals("click")) {
                    sttp.getDriver().findElement(By.cssSelector(lv.getRobotValueAttribute())).click();
                } else if (lv.getActionType().equals("select")) {
                    WebElement element = sttp.getDriver().findElement(By.cssSelector(lv.getRobotValueAttribute()));
                    Select select = new Select(element);
                    select.selectByValue(lv.getRobotValueField());
                } else if (lv.getActionType().equals(("getText"))) {
                    sttp.getDriver().findElement(By.cssSelector(lv.getRobotValueAttribute())).getText();
                }

            } else {
                lv.setStatus("erro");
                Evidence.evidenceResult(sttp, lv);
                return false;
            }
        }

        lv.setStatus("pass");

        return true;

    }
}